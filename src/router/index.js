
const Home = resolve =>require(['../components/Home'],resolve)
//import Home from '../components/Home'
import preaching from'../components/preaching' //理论宣讲
import educational from'../components/educational' //教育实践
import cultural from'../components/cultural' //文化服务
import science from'../components/science' //科技科普
import fitness from'../components/fitness' //健身体育
import volunteering from'../components/volunteering' //志愿活动


import preachPage from '../components/navPage/preachPage'  //二级界面
import educationPage from '../components/navPage/educationPage'
import culturPage from '../components/navPage/culturPage'
import sciencePage from '../components/navPage/sciencePage'
import fitnessPage from '../components/navPage/fitnessPage'
import voultPage from '../components/navPage/voultPage'

//二级路由
import pageWordList from '../components/navPage/pageWordList';  //纯文字列表
import pagePicList from '../components/navPage/pagePictureList' //图文列表



//详情路由
import wordDetail from '../components/detail/wordDetail';  //详情页 支持tpe 1234
import ActiveDetail from '../components/detail/activeDetail';//详情页 支持活动



export const routes =[
    {path:'/',name:'homeLink',component:Home},  //首页 
    {path:'/preaching/:id',name:'preachingLink',component:preaching}, //理论宣讲
    {
      path:'/preachPage/:id/:linkName/:childId/:dataType',
      name:'preachPageLink',
      component:preachPage,
      children:[
        {path:'/preachPoint/:id/:dataType',name:'preachPointLink',component:pagePicList},  //点单理论
        {path:'/preachInfor/:id/:dataType',name:'preachInforLink',component:pageWordList},  //理论资讯
        {path:'/preachBiography/:id/:dataType',name:'preachBiographyLink',component:pageWordList},  //金山理论讲习所
        {path:'/preachKnowledge/:id/:dataType',name:'preachKnowledgeLink',component:pageWordList},  //公开课
        {path:'/themeDay/:id/:dataType',name:'themeDayLink',component:pageWordList},  //主播党日
        {path:'/broadcasting/:id/:dataType',name:'broadcastingLink',component:pageWordList},  //党建播报
        {path:'/preachNotice/:id/:dataType',name:'preachNoticeLink',component:pageWordList},  //通知公告
      ]
    },
    {path:'/preachDetail/:id/:dataType/:type',name:'preachDetailLink',component:wordDetail}, //理论宣讲资讯详情
    {path:'/preachActive/:id',name:'preachActiveLink',component:ActiveDetail}, //理论宣讲活动详情
    {path:'/educational/:id',name:'educationalLink',component:educational}, //教育实践
    {
      path:'/educaPage/:id/:linkName/:childId/:dataType',
      name:'educaPageLink',
      component:educationPage,
      children:[
        {path:'/educaInfor/:id/:dataType',name:'educaInforLink',component:pageWordList},  //教育资讯
        {path:'/educaModel/:id/:dataType',name:'educaModelrLink',component:pageWordList},  //国学讲堂
        //{path:'/educaGood/:id/:dataType',name:'educaGoodLink',component:pageWordList},  //丹阳好人
       // {path:'/educaFamily/:id/:dataType',name:'educaFamilyLink',component:pageWordList},  //最美家庭
        {path:'/educaGrowth/:id/:dataType',name:'educaGrowthLink',component:pageWordList},  //文明伴成长
        {path:'/educaHarmonious/:id/:dataType',name:'educaHarmoniousLink',component:pageWordList},  //文明促和谐
        {path:'/educaRevitalization/:id/:dataType',name:'educaRevitalizationLink',component:pagePicList},  //文明助振兴
        {path:'/educaExposure/:id/:dataType',name:'educaExposureLink',component:pageWordList},  //曝光台
        {path:'/educaNotice/:id/:dataType',name:'educaNoticeLink',component:pageWordList},  //通知公告
        {path:'/educaPoint/:id/:dataType',name:'educaPointLink',component:pagePicList},  //点单教育
      ]
    },
    {path:'/educahDetail/:id/:dataType/:type',name:'educahDetailLink',component:wordDetail}, //教育实践详情
    {path:'/educaActive/:id',name:'educaActiveLink',component:ActiveDetail}, //教育实践活动详情
    {path:'/cultural/:id',name:'culturalLink',component:cultural},    //文化服务
    {
      path:'/culturPage/:id/:linkName/:childId/:dataType',
      name:'culturPageLink',
      component:culturPage,
      children:[
        {path:'/culturInfor/:id/:dataType',name:'culturInforLink',component:pageWordList},  //文化资讯
        {path:'/culturForum/:id/:dataType',name:'culturForumLink',component:pageWordList},  //曲阿大讲堂
        {path:'/culturOpera/:id/:dataType',name:'culturOperaLink',component:pageWordList},  //戏曲惠民
        {path:'/culturExhibition/:id/:dataType',name:'culturExhibitionLink',component:pageWordList},  //文化展会
        {path:'/culturDance/:id/:dataType',name:'culturDanceLink',component:pageWordList},  //广场舞
        {path:'/culturBank/:id/:dataType',name:'culturBankLink',component:pageWordList},  //文明银行
        {path:'/culturNotice/:id/:dataType',name:'culturNoticeLink',component:pageWordList},  //通知公告
        {path:'/culturPoint/:id/:dataType',name:'culturPointLink',component:pagePicList}  //点单文化
      ]
    },
    {path:'/culturDetail/:id/:dataType/:type',name:'culturDetaillLink',component:wordDetail}, //文化服务详情
    {path:'/culturActive/:id',name:'culturActiveLink',component:ActiveDetail}, //文化服务活动详情
    {path:'/science/:id',name:'scienceLink',component:science},      //科技科普
    {
      path:'/scienPage/:id/:linkName/:childId/:dataType',
      name:'scienPageLink',
      component:sciencePage,
      children:[
        {path:'/scienInfor/:id/:dataType',name:'scienInforLink',component:pageWordList},  //科普资讯
        {path:'/scienForum/:id/:dataType',name:'scienForumLink',component:pageWordList},  //科普讲堂
        {path:'/scienBreed/:id/:dataType',name:'scienBreedLink',component:pageWordList},  //科学养殖
        {path:'/scienReview/:id/:dataType',name:'scienReviewLink',component:pageWordList},  //乡村评议
        {path:'/scienNotice/:id/:dataType',name:'scienNoticeLink',component:pageWordList},  //通知公告
        {path:'/scienPoint/:id/:dataType',name:'scienPointLink',component:pagePicList}  //点单科普
      ]
    },
    {path:'/scienDetaill/:id/:dataType/:type',name:'scienDetaillLink',component:wordDetail}, //科技科普详情
    {path:'/scienActive/:id',name:'scienActiveLink',component:ActiveDetail}, //科技科普活动详情
    {path:'/fitness/:id',name:'fitnessLink',component:fitness},  //健身体育
    {
      path:'/fitPage/:id/:linkName/:childId/:dataType',   
      name:'fitPageLink',
      component:fitnessPage,
      children:[
        {path:'/fitInfor/:id/:dataType',name:'fitInforLink',component:pageWordList},  //体育资讯
        {path:'/fitBasekt/:id/:dataType',name:'fitBasektLink',component:pageWordList},  //篮球城市
        {path:'/fitTown/:id/:dataType',name:'fitTownLink',component:pageWordList},  //一镇一品
        {path:'/fitNational/:id/:dataType',name:'fitNationalLink',component:pageWordList},  //全民健身
        {path:'/fitShare/:id/:dataType',name:'fitShareLink',component:pageWordList},  //场所共享
        {path:'/fitNotice/:id/:dataType',name:'fitNoticeLink',component:pageWordList},  //通知公告
        {path:'/fitPoint/:id/:dataType',name:'fitPointLink',component:pagePicList}  //点单体育
      ]
    },
    {path:'/fitDetaill/:id/:dataType/:type',name:'fitDetaillLink',component:wordDetail}, //健身体育详情
    {path:'/fitActive/:id',name:'fitActiveLink',component:ActiveDetail}, //健身体育活动详情
    {path:'/volunteering/:id',name:'volunteeringLink',component:volunteering},  //志愿活动
    {
      path:'/voluntPage/:id',
      name:'voluntPageLink',
      component:voultPage,
      children:[
        {path:'/voluntInfor/:id/:dataType',name:'voluntInforLink',component:pagePicList},  //活动信息
        {path:'/voluntTrain/:id/:dataType',name:'voluntTrainLink',component:pageWordList},  //志愿者培训
        {path:'/voluntElegant/:id/:dataType',name:'voluntElegantLink',component:pageWordList},  //志愿者风采
        {path:'/voluntTeam/:id/:dataType',name:'voluntTeamLink',component:pagePicList},  //志愿者团体
        {path:'/voluntNotice/:id/:dataType',name:'voluntNoticeLink',component:pageWordList},  //通知公告
        {path:'/voluntGlimpse/:id/:dataType',name:'voluntGlimpseLink',component:pageWordList},  //活动掠影
        {path:'/voluntOrder/:id/:dataType',name:'voluntOrderLink',component:pagePicList}  //点单服务 
      ]
    },
    {path:'/voluntDetaill/:id/:dataType/:type',name:'voluntDetaillLink',component:wordDetail}, //志愿活动详情
    {path:'/voluntActive/:id',name:'voluntActiveLink',component:ActiveDetail}, //志愿活动详情
    {
      path:'*',
      redirect:'./'
    }

]