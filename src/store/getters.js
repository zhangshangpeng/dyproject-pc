//获取属性和状态
export const currentUser = state => state.currentUser
export const header = state => state.header
export const access_token = state => state.access_token
export const isLogin = state => state.isLogin