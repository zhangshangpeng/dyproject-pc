
    //登录的状态信息
    export const userName = (state,user) => {
        if(user){
            state.currentUser = user
            state.isLogin = true
        } else{
            state.currentUser = null
            state.isLogin = false
        }
    }
    export const userHeader = (state,user) => {
        if(user){
            state.header = user
            state.isLogin = true
        } else{
            state.header = null
            state.isLogin = false
        }
    }
    export const userToken = (state,user) => { 
        if(user){
            state.access_token = user 
        } else{
            state.access_token = null
        }
    }



    