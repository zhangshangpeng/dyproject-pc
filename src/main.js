import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import {routes} from './router'
import axios from 'axios'
import {store} from './store'
import global_ from './components/Global'
// 全局引入Toast
import './components/Toast/toast.css';
import Toast from './components/Toast/index';
Vue.use(Toast);
import $ from 'jquery'


Vue.use(VueRouter)
  //axios.defaults.baseURL="http://dycms.smallby.cn/"//开发后台接口
  axios.defaults.baseURL="http://dycms.cst-info.cn"//线上后台接口
//配置vue原型(在任何组件可正常使用)
Vue.prototype.http = axios
Vue.prototype.Global = global_

  const router = new VueRouter({
    routes,
    //mode:'history',
    scrollBehavior(to,from,sacedPosition){
      // 高100的位置 return{ x:0,y:100}
      // btn的位置 return {selector:'.btn'}
      //记住上一次划到的位置
      // if(savedPosition){
      //   return savedPosition
      // }else{
      //   return {x:0,y:0}
      // }
    }
  })

  //全局守卫
  //  router.beforeEach((to,from,next)=>{
  //   //  alert('还没有登陆，请先登陆!')
  //   //  next()
  //   //  console.log(to)
  //   //  判断store.gettes.isLogin === false
  //   if(to.path == '/login' || to.path == '/register'){
  //     next();
  //   }else{
  //     alert('还没有登陆，请先登陆!')
  //     next('/login');
  //   }
  //  })

  //后置钩子
  // router.afterEach((to,from,next) => {
  //   window.scrollTo(0,0);
  // });

  new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
